'use strict';


angular.module('daDirectives', [])
    .directive("scroll", function () {
        return function (scope, element, attrs) {
            angular.element(element).bind("scroll", function () {
                scope[attrs.scroll] = true;
                scope.$apply();
            });
        };
    })
    .directive('colorbox', function () {
        return {
            restrict: 'AC',
            link: function (scope, element, attrs) {
                $(element).colorbox(attrs.colorbox);
            }
        };
    })
    .directive('iframecolorbox', function () {
        return {
            restrict: 'AC',
            link: function (scope, element, attrs) {
                $(element).colorbox({iframe: true, innerWidth: 800, innerHeight: 600});
            }
        };
    })
    .directive('menu', function ($compile) {
        return {
            restrict: 'E',
            transclude: true,
            scope: {},
            controller: function ($scope, $element, Global) {
                $scope.global = Global;
                var menu_items = $scope.menu_items = [];

                $scope.select = function (menu_item) {
                    angular.forEach(menu_items, function (menu_item) {
                        menu_item.selected = false;
                    });
                    menu_item.selected = true;
                    $scope.current_menu_item = menu_item;
                    $scope.global.current_menu_item = menu_item;
                }

                $scope.next = function () {
                    angular.forEach(menu_items, function (menu_item) {
                        menu_item.selected = false;
                    });
                    var next_menu_item_index = menu_items.indexOf($scope.current_menu_item) + 1;
                    if (next_menu_item_index == menu_items.length) {
                        var next_menu_item = menu_items[0];
                    } else {
                        var next_menu_item = menu_items[next_menu_item_index];
                    }
                    next_menu_item.selected = true;
                    $scope.current_menu_item = next_menu_item;
                }

                $scope.prev = function () {
                    angular.forEach(menu_items, function (menu_item) {
                        menu_item.selected = false;
                    });
                    var prev_menu_item_index = menu_items.indexOf($scope.current_slide) - 1;
                    if (prev_menu_item_index < 0) {
                        var prev_menu_item = menu_items[(menu_items.length - 1)];
                    } else {
                        var prev_menu_item = menu_items[prev_menu_item_index];
                    }
                    prev_menu_item.selected = true;
                    $scope.current_menu_item = prev_menu_item;
                }

                this.addMenuItem = function (menu_item) {
                    if (menu_items.length == 0) $scope.select(menu_item);
                    menu_items.push(menu_item);
                }
            },
            template: '<ul class="menu">' +
                '<div class="menu_wrapper" ng-transclude ></div>' +
//                '<li class="menu_item"  ng-class="{active:menu_item.selected}"  ng-repeat="menu_item in menu_items" >((scope.href))<a ng-click="select(menu_item);">((menu_item))</a></li>' +
//                '<div class="pager">' +
//                '<a href="" ng-click="select(menu_item);" ng-repeat="menu_item in menu_items" ng-class="{active:menu_item.selected}">(($index+1))</a>' +
//                '</div>' +
//                '<a class="prev misc_icons-prev" ng-click="prev();">предыдущий</a>' +
//                '<a class="next misc_icons-next" ng-click="next();">следующий</a>' +
                '</ul>',
            replace: true
        };
    })
    .directive('menuitem', function () {
        return {
            require: '^menu',
            restrict: 'E',
            transclude: true,
            scope: {
                href: '@',
                rel: '@'
            },
            link: function ($scope, $element, $attrs, da_menuCtrl) {
                da_menuCtrl.addMenuItem($scope);
            },
//            template: '<li class="menu_item"  ng-class="{active:selected}"><a ng-transclude></a></li>',
            template: '<li class="menu_item"><a ng-transclude></a></li>',
            replace: true
        };
    })
    .directive('hrScrollLoad', function () {
        return {
            restrict: 'A',
            scope: {
                class: '@'
            },
            transclude: true,
            replace: true,
            template: '<div class="{? class ?}" infinite-scroll="load_next_page()" infinite-scroll-distance="2" ng-transclude></div>',
            controller: function ($scope, Global, $http, $location, cfg, $compile, $element) {
                $scope.global = Global;
                $scope.global.next_page = 2;
                $scope.global.end_list = false;


                $scope.load_next_page = function (insert_to) {
                    if ($scope.global.busy || $scope.global.end_list) return;

                    $scope.global.busy = true;
                    console.log($location.absUrl() + '?page=' + $scope.global.next_page);


                    $http({
                        method: 'POST',
                        url: $location.absUrl(),
                        params: {
                            'page': $scope.global.next_page,
                            'tag': $scope.global.current_tag_slug
                        },
                        headers: {'Content-Type': 'application/json'}
                    }).success(function (data) {
                            if (data == 'end') {
                                console.log('end');
                                $scope.global.end_list = true;
                                $scope.global.busy = false;
                            } else {
                                $scope.global.next_page = $scope.global.next_page + 1;
                                $scope.global.busy = false;

                                var compile_data = $compile(data)($scope);

                                angular.element($element).append(compile_data);
                            }

                        }, function error(response) {
                            console.log('error');
                            $scope.global.end_list = true;
                        }
                    );
                }
            }
        };
    })
    .directive('ngInitial', function ($parse) {
        return {
            restrict: "A",
            compile: function ($element, $attrs) {
                var initialValue = $attrs.value || $element.val();
                return {
                    pre: function ($scope, $element, $attrs) {
                        $parse($attrs.ngModel).assign($scope, initialValue);
                    }
                }
            }
        }
    })
    .directive('slideshow', function () {
        return {
            restrict: 'E',
            transclude: true,
            scope: {},
            controller: function ($scope, $element) {
                var slides = $scope.slides = [];
                $scope.slides_left = 0;
                $scope.window_width = window.innerWidth;
                $scope.is_prev = 1;
                $scope.is_next = 1;

                console.log($scope.slides_left);

                $scope.select = function (slide, index) {
                    angular.forEach(slides, function (slide) {
                        slide.active = false;
                    });
                    slide.active = true;
                    $scope.current_slide = slide;
                    $scope.slides_left = $scope.window_width * index * -1;

                }

                $scope.next = function () {
                    angular.forEach(slides, function (slide) {
                        slide.active = false;
                    });
                    var next_slide_index = slides.indexOf($scope.current_slide) + 1;
                    if (next_slide_index == slides.length) {
                        var next_slide = slides[0];
                    } else {
                        var next_slide = slides[next_slide_index];
                    }
                    next_slide.active = true;
                    $scope.current_slide = next_slide;

//                    if ($scope.slides_left <= $scope.window_width * slides.length * -1 + $scope.window_width) {
////                        $scope.slides_left = 0
//                    } else {
//                        $scope.slides_left = $scope.slides_left - $scope.window_width;
//                    }
//
//                    if ($scope.slides_left < $scope.window_width * slides.length * -1 + ($scope.window_width * 2)) {
//                        $scope.is_next = 0;
//                    } else {
//                        $scope.is_next = 1;
//                    }
//                    if (($scope.slides_left + $scope.window_width) > 0) {
//                        $scope.is_prev = 0;
//                    } else {
//                        $scope.is_prev = 1;
//                    }
                }

                $scope.prev = function () {
                    angular.forEach(slides, function (slide) {
                        slide.active = false;
                    });
                    var prev_slide_index = slides.indexOf($scope.current_slide) - 1;
                    if (prev_slide_index < 0) {
                        var prev_slide = slides[(slides.length - 1)];
                    } else {
                        var prev_slide = slides[prev_slide_index];
                    }
                    prev_slide.active = true;
                    $scope.current_slide = prev_slide;

//                    if ($scope.slides_left >= 0) {
////                      $scope.slides_left = $scope.window_width * slides.length * -1 + $scope.window_width
//                    } else {
//                        $scope.slides_left = $scope.slides_left + $scope.window_width;
//                    }
//
//                    if ($scope.slides_left < $scope.window_width * slides.length * -1 + ($scope.window_width * 2)) {
//                        $scope.is_next = 0;
//                    } else {
//                        $scope.is_next = 1;
//                    }
//                    if (($scope.slides_left + $scope.window_width ) > 0) {
//                        $scope.is_prev = 0;
//                    } else {
//                        $scope.is_prev = 1;
//                    }
                }

                this.addSlide = function (slide) {
                    if (slides.length == 0) $scope.select(slide, 0);
                    slides.push(slide);
                }
            },
            template: '<div class="slideshow">' +
                //'<div class="slideshow_wrapper" ng-transclude ng-swipe-left="next();" ng-swipe-right="prev();" ng-style="{width: slides.length * window_width, left: slides_left}"></div>' +
                '<div class="slideshow_wrapper" ng-transclude ng-swipe-left="next();" ng-swipe-right="prev();" ng-style=""></div>' +
                '<div class="pager">' +
                '<a href="" class="page" ng-click="select(slide, $index);" ng-repeat="slide in slides" ng-class="{active:slide.active}">{{$index+1}}</a>' +
                '</div>' +
                '<a class="prev misc_icons-prev" ng-click="prev();" ng-show="is_prev">предыдущий</a>' +
                '<a class="next misc_icons-next" ng-click="next();" ng-show="is_next">следующий</a>' +
                '</div>',
            replace: true
        };
    })
    .directive('slide', function () {
        return {
            require: '^slideshow',
            restrict: 'E',
            transclude: true,
            scope: {
                title: '@',
                slideId: '@'
            },
            link: function (scope, element, attrs, da_slideshowCtrl) {
                da_slideshowCtrl.addSlide(scope);
                scope.window_width = window.innerWidth;
            },
//          template: '<div class="slide animate-show"  ng-show="active" ng-transclude></div>',
//            template: '<div class="slide animate-show"  ng-transclude ng-style="{width: window_width}"></div>',
            template: '<div class="slide animate-show" ng-class="{active:active}" ng-transclude ng-style=""></div>',
            replace: true
        };
    });
