$(window).ready(function () {

    if (window.PIE) {
        $('.pie').each(function () {
            PIE.attach(this);
        });
    }

    var page_title = $('h2#page-title').text();
    $('ul.menu li:contains(' + page_title + ') a').addClass('current');

    $(".gallery.cbox a.photo, a.cbox").colorbox();
    $(".youtube").colorbox({iframe: true, innerWidth: 853, innerHeight: 480});


    $("a[href^='http://']").attr("target", "_blank");

    $("#sortable").sortable({
        handle: ".sort_handle",
        placeholder: "ui-state-highlight",
        update: function (event, ui) {
            var data = $(this).sortable('serialize', {attribute: 'position'});
            console.log(ui);
            $.ajax({
                data: data,
                type: 'POST',
                url: '/your/url/here'
            });
        }
    });
    //$( "#sortable" ).disableSelection();

});


//-------------------------------------------------------------------------------
//AJAX
//-------------------------------------------------------------------------------

$(document).ajaxComplete(function () {
    $("a[href^='http://']").attr("target", "_blank");
    console.log('ajax complete!');


});

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
        vars[key] = value;
    });
    return vars;
}

function setCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    }
    else var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}

function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function deleteCookie(name) {
    setCookie(name, "", -1);
}

