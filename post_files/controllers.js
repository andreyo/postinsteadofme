'use strict';

//var API_URL = 'http://ayvazovsky.webfactional.com/';
var API_URL = 'http://127.0.0.1:8000/';
//var API_URL = 'http://triton_ua.in.ua/';
//var API_URL = 'http://triton_ua.webfactional.com/';
var PORT = ':8000';
//var PORT = '';


function menuCtr($scope, Global, $http, $location, cfg) {
    $scope.global = Global;
}

function socialsCtrl($scope, Global, $http, $location, cfg) {
    $scope.global = Global;
}

function iconsCtrl($scope, Global, $http, $location, cfg) {
    $scope.global = Global;
}

function modalCtrl($scope, Global, $http, $location) {
    $scope.global = Global;
}

function formCtrl($scope, Global, $http, $location) {
    $scope.global = Global;
}

function landingCtrl($scope, Global, $http, $location, $interval, cfg) {
    $scope.global = Global;
}

function pagepartCtrl($scope, Global, $http, $location) {
    $scope.global = Global;

    $scope.save = function () {
        $http.get('http://' + $location.host() + ":" + $location.port() + '/pagepart/save/' + $scope.id).success(function (data) {
        });
    }


    $scope.delete = function () {
        $http.get('http://' + $location.host() + ":" + $location.port() + '/pagepart/delete/' + $scope.$parent.id + '/' + $scope.id).success(function (data) {
            console.log(data);
        });
    }

    $scope.publish = function () {
        $http.get('http://' + $location.host() + ":" + $location.port() + '/pagepart/publish/' + $scope.id).success(function (data) {
        });
    }

    $scope.unpublish = function () {
        $http.get('http://' + $location.host() + ":" + $location.port() + '/pagepart/unpublish/' + $scope.id).success(function (data) {
        });
    }

    $scope.clone = function () {
        $http.get('http://' + $location.host() + ":" + $location.port() + '/pagepart/clone/' + $scope.$parent.id + '/' + $scope.id).success(function (data) {
        });
    }

    $scope.add = function () {
        console.log($scope.$parent.id + '/' + $scope.id);
        $http.get('http://' + $location.host() + ":" + $location.port() + '/pagepart/add/' + $scope.$parent.id + '/' + $scope.id).success(function (data) {
            console.log(data);
        });
    }

}


function toTop($scope, Global, $http, $location, cfg, $anchorScroll) {
    $scope.global = Global;

    $scope.to_top = function () {
        $location.hash('container');
        $anchorScroll();
    }
}


function mainCtrl($scope, Global, $http, $location, $interval, cfg) {
    $scope.global = Global;

    //var interval = $interval(function () {
//        TEL HREF FOR CALL TOUCH
//        var main_phone = angular.element('#main_phone').html();
//        console.log('main_phone', main_phone);
//        if (main_phone != $scope.global.phone_href) {
//            $scope.global.phone_href = main_phone;
//            angular.element('.json_phone').html(main_phone);
//            angular.element('.json_phone_href').attr('href', 'tel:' + main_phone);
//        }
//    }, 1000);

}