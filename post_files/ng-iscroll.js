/*!
Copyright (c) 2013 Brad Vernon <bradbury.vernon@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/
angular.module("ng-iscroll",[]).directive("ngIscroll",function(){return{replace:!1,restrict:"A",link:function(e,t,n){function a(){e.$parent.myScroll===undefined&&(e.$parent.myScroll=[]),e.$parent.myScroll[s]=new iScroll(t[0],i)}var r=5,i={snap:!0,momentum:!0,hScrollbar:!1},s=n.ngIscroll;s===""&&(s=n.id),n.ngIscrollForm!==undefined&&n.ngIscrollForm=="true"&&(i.useTransform=!1,i.onBeforeScrollStart=function(e){var t=e.target;while(t.nodeType!=1)t=t.parentNode;t.tagName!="SELECT"&&t.tagName!="INPUT"&&t.tagName!="TEXTAREA"&&e.preventDefault()});if(e.$parent.myScrollOptions)for(var o in e.$parent.myScrollOptions)if(o===s)for(var u in e.$parent.myScrollOptions[o])i[u]=e.$parent.myScrollOptions[o][u];else i[o]=e.$root.myScrollOptions[o];n.ngIscrollDelay!==undefined&&(r=n.ngIscrollDelay),e.$watch(n.ngIscroll,function(){setTimeout(a,r)}),e.$watch(n.ngIscrollList,function(){e.$parent.myScroll[s].refresh()})}}});