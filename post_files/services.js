angular.module('daServices', ['ngResource'])
    .factory('Question', function ($resource, $location, cfg) {
        return $resource('http://' + $location.host() + cfg.PORT + '/api/questions/?format=json', {
            id: '@id',
            start: '@start',
            num: '@num'
        }, {
            all: {method: 'GET', params: {id: ''}}
        });
    })
    .factory('Tags', function ($resource, $location) {
        console.log('http://' + $location.host() + ':' + $location.port() + '/crud/tags');
        return $resource('/crud/tags/', {}, {
            all: {method: 'GET', isArray: true}
        });
    })
//    .factory('Comments', function ($resource, cfg) {
//        return $resource(cfg.API_URL + 'comments/:id/:start/:num/:method', {
//            id: '@id',
//            start: '@start',
//            num: '@num',
//            method: ''
//        }, {
//            get: {method: 'GET', cache : false},
//            edit: {method: 'PUT'},
//            delete_comment: {method: 'POST', try_method: 'delete'},
//            save: {method: 'POST', params: {object_id: '@object_id', text: '@text'}}
////            post: {method: 'POST', params: {uid: '1', text: '@text'}}
//        });
//    })
    .factory('Global', function ($http) {
        return {
            callback_form_show: '0',
            quick_order_form_show: '0',
            credit_order_form_show: '0',
            comment_form_show: '0',
            quick_product: "",
            cart: ""
        };
    });
;


//http://estatistics.ru/mcu/callback/index.php?r=friends/delete/uid/777
//http://estatistics.ru/mcu/callback/index.php?r=friends/get/fnum/16/start/0/