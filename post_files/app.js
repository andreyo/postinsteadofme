'use strict';

var da_app = angular.module('daApp', ['ngAnimate', 'ngRoute', 'ngSanitize', 'daServices', 'daDirectives', 'daFilters', 'daCfg', 'daTabor','ui.bootstrap', 'yaMap', 'angularFileUpload'], function ($interpolateProvider) {
    $interpolateProvider.startSymbol('{?');
    $interpolateProvider.endSymbol('?}');
});

angular.module('daCfg', [])
    .constant('cfg', {
        'API_URL': 'http://mcu.sweetdrinks.ru/callback/index.php/api2/',
        'PORT': ':8000'
    })
